// 1. When should we define something as an object vs an array ?

console.log("1. If we need to simply list various things inside a certain place we use Array. Where as if we are to save various properties inside a certain place we use Object.");


// 2. What is scope ? What is global scope vs local scope?

console.log("2. Any variable defined are known as scope. If a variable is defined outside a certain function then it is known as global scope, whereas if a variable is defined within a function it is known as local scope.");

// 3. In this sentence "I love cats, cats are mythological creatures in egypt. In the movie mummy a cat scared away the mummy"
//   - count the occurences of cats in the above sentence
//   - Replace all occurences of the word "cat" with "dog" in the following sentence "I love cats, cats are mythological creatures in egypt. In the movie mummy a cat scared away the mummy"

var replaceCtwithDog = function(){
	var sentence = "I love cats, cats are mythological creatures in egypt. In the movie mummy a cat scared away the mummy.";
	var countCats = (sentence.match(/cats/gi) || []).length;
	console.log("No. of cats occurance: " + countCats);
	var replaceCats = (sentence.replace(/cats/gi, "dog"));
	console.log(replaceCats);

};
replaceCtwithDog();

// 4. Create a grocery list:
//   - create function that adds items to grocery list
//   - create function that removes items from grocery list
//     - function to remove must be able to remove item by name of item e.g tomato , potato etc.


var groceryList = ['tomato', 'apple', 'onion', 'egg', 'potato'];
console.log(groceryList);	
var addGrocery = function(item) {
	groceryList.push(item);
};
addGrocery('garlic');
console.log(groceryList);
var removeGrocery = function() {
	var identifyGroceryPosition = groceryList.indexOf('tomato');
	groceryList.splice(identifyGroceryPosition, 1);
};
removeGrocery();
console.log(groceryList);

// 5. I have a bike:
//   a. I need functionality to store bike details
//     - make
//     - model
//     - distance travelled
//     - fuel capacity
//   b. Calculate mileage
//   c. Given a distance to be travelled e.g. like 10km calculate if bike can travel that distance.
//   d. Create a list of problems the bike has run into and record it.
//   e. create a function that allows user to see what the make, model, distance travelled etc. of the bike is.

var totalDistance = 100;
var totalPetrol = 1;

var myBike = {
	make: 'Honda',
	model: 'Stunner 125',
	distanceTravelled: 500,
	fuelCapacity: 10,
	mileage: function () {
		return (this.distanceTravelled / this.fuelCapacity);
	},
	enoughPetrol: function() {
		if (totalDistance / totalPetrol <= this.mileage()) {
			return ("Yes you have enough petrol to reach destination");
		}
		else {
			return ("No you don't have enough petrol to reach destination");
		}
	},
	seeDetail: function () {
		var detailType = prompt('Enter detail you want to know.');
		if (detailType == "make") {
			return ("Bike make is: " + this.make);
		}
		else if (detailType == "model") {
			return ("Bike model is: " + this.model);
		}
		else if (detailType == "distance travelled") {
			return ("Bike distance travelled is: " + this.distanceTravelled + "km");
		}
		else if (detailType == "fuel capacity") {
			return ("Bike fuel capacity is: " + this.fuelCapacity + "ltr");
		}
		else {
			return ("Incorrect detail name please enter either make or model or distance travelled or fuel capacity");
		}
	}
};
// var milageResult;

// var calculateMilage = function() {
// 	milageResult = myBike.distanceTravelled / myBike.fuelCapacity;
// 	console.log("Total Milage: " + milageResult);
// };
// calculateMilage();
console.log("Total Milage: " + myBike.mileage());
console.log("Total Milage: " + myBike.enoughPetrol());
console.log(myBike.seeDetail());

// var enoughFuel = function() {
// 	if (totalDistance / totalPetrol <= milageResult) {
// 		console.log("Yes you have enough petrol to reach destination");
// 	}
// 	else {
// 		console.log("No you don't have enough petrol to reach destination");
// 	}
// };
// enoughFuel();

// var seeDetail = function () {
	// var detailType = prompt('Enter detail you want to know.');
	// if (detailType == "make") {
	// 	console.log("Bike make is: " + myBike.make);
	// }
	// else if (detailType == "model") {
	// 	console.log("Bike model is: " + myBike.model);
	// }
	// else if (detailType == "distance travelled") {
	// 	console.log("Bike distance travelled is: " + myBike.distanceTravelled + "km");
	// }
	// else if (detailType == "fuel capacity") {
	// 	console.log("Bike fuel capacity is: " + myBike.fuelCapacity + "ltr");
	// }
	// else {
	// 	console.log("Incorrect detail name please enter either make or model or distance travelled or fuel capacity");
	// }
// };
// seeDetail();

// 6. Create a function that returns the sum of max and min value of a given list of numbers.
// e.g if the numbers given were 1,2,3,4 the result would be 1+4 = 5
//   - Bonus: create a form that takes the value of multiple numbers upto 5 and returns the sum of the max and minimum number. [jQuery not allowed]

var minMaxSum = function(){
	var minMaxArray = [1, 2, 3, 4, 5];
	var maxNumber = Math.max.apply(null, minMaxArray);
	// console.log(maxNumber);
	var minNumber = Math.min.apply(null, minMaxArray);
	// console.log(minNumber);
	var sumMinMax = maxNumber + minNumber;
	console.log(sumMinMax);
};
minMaxSum();